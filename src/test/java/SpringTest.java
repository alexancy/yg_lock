import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.yonggan.test.App;

public class SpringTest {

	@SuppressWarnings("resource")
	@Test
	public void test() throws InterruptedException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-context.xml");

		final App bean = ctx.getBean(App.class);

		for (int i = 0; i < 20000; i++) {

			new Thread(new Runnable() {

				@Override
				public void run() {
					bean.print();

				}
			}).start();;
		}
		
		
		Thread.sleep(10000);
	}

}
