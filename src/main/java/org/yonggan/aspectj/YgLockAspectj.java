package org.yonggan.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.yonggan.cache.JedisLockContext;

@Aspect
@Component
@EnableAspectJAutoProxy
public class YgLockAspectj {

	@Pointcut("@annotation(org.yonggan.annotation.YgLock)")
	public void ygLock() {
	}

	@Around("ygLock()")
	public Object aroundExec(ProceedingJoinPoint pjp) throws Throwable {

		// 实现加锁 。。。
		JedisLockContext.loadJedisInstance();
		try {
			return pjp.proceed();
		} catch (Throwable e) {
			throw e;
		} finally {
			// 锁的释放
			JedisLockContext.releaseJeids();
		}
	}

}
