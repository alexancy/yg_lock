package org.yonggan.test;

import org.springframework.stereotype.Component;
import org.yonggan.annotation.YgLock;

@Component
public class App {

	private static int i = 1000;

	public void print() {

		if (i <= 0) {
			return;
		}
		i--;
		System.out.println("。。业务代码。。。" + i);
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
