package org.yonggan.cache;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * 使用redis 锁
 * 
 * @author Mr.YongGan.Zhang
 *
 */
@Component
public class JedisLockContext {

	private static final Logger LOGGER = Logger.getLogger(JedisLockContext.class);

	private static ThreadLocal<JedisHolder> itemThreadLocal = new ThreadLocal<>();

	private static JedisPool jedisPool;

	@SuppressWarnings("unused")
	private static class JedisHolder {
		
		private static final String YG_LOCK_KEY = "YG_LOCK_KEY";

		private Jedis instance;

		private volatile AtomicInteger atomicInt = new AtomicInteger();

		public JedisHolder(Jedis instance) {
			this.instance = instance;
			countInc();
		}

		public void countInc() {
			String lockValue = atomicInt.incrementAndGet()+"";
//			System.out.println(lockValue);
//			instance.set(YG_LOCK_KEY,lockValue);
		}

		public void countDec() {
			String string = instance.get(YG_LOCK_KEY);
//			System.out.println(string);
//			atomicInt.decrementAndGet();
		}

		public boolean canRelase() {
			return atomicInt.get() == 0;
		}

		public Jedis getInstance() {
			return instance;
		}

		public void setInstance(Jedis instance) {
			this.instance = instance;
		}
	}

	public static void loadJedisInstance() {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("JedisLockContext loadJedisInstance()");
		}

		if (itemThreadLocal.get() == null) {
			JedisHolder data = new JedisHolder(jedisPool.getResource());
			itemThreadLocal.set(data);
		} else {
			JedisHolder holder = itemThreadLocal.get();
			holder.countInc(); // 加上锁
		}
	}

	public static Jedis getJedis() {
		return itemThreadLocal.get().getInstance();
	}

	/**
	 * 并发锁的 释放
	 */
	public static void releaseJeids() {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("JedisLockContext releaseJeids()");
		}
		JedisHolder holder = itemThreadLocal.get();
		// 释放
		if (null != holder && holder.canRelase()) {
			holder.getInstance().close();// 将redist 关闭
			itemThreadLocal.remove(); // 将
		}
	}

	@Component
	public static class JedisPoolInjector {

		@Autowired(required = false)
		private JedisPool jedisPool;

		@PostConstruct
		public void init() {
			JedisLockContext.jedisPool = jedisPool;
		}
	}

}
